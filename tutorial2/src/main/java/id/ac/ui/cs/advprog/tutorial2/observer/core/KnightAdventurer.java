package id.ac.ui.cs.advprog.tutorial2.observer.core;
public class KnightAdventurer extends Adventurer {
	public KnightAdventurer(String name, Guild guild) {
		super(name,guild);
	}
	
	public void update() {
		this.getQuests().add(this.guild.getQuest());
	}
}