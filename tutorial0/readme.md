# Tutorial 0

# I Got Transported to Another World and All I Can Do is Code with My Advanced Programming Knowledge

You open your eyes and find yourself in a dimly lit room. You try and get your bearings as your eyes slowly adjust to the darkness.

The air is calm, as if no wind was blowing at all. 
With your vision now clearer, you spot a figure at the end of the room. They appear to be perturbed.

They stare at you pleadingly, inching closer with every second. You can hear their footsteps loud and clear, but it appears as if they were walking on nothing. The footsteps grow louder as they approach.

Suddenly you hear a voice inside of your head. It was an anxious voice, and it seemed to be transmitted directly into your mind by the figure.
The figure tells you that they had summoned you here, into a world very different from the one you are familiar with. They apologize for the inconvenience, and state that they needed your help.

They introduce themselves as []. You don’t quite catch the name.
[] explains that in this parallel universe, there exists an ancient magic language known as java. Humans in this world understand how to use this java magic. They have created many things with java. Someone has even managed to make a web, whatever that may be, using the magic.

One of the many arcane magic used by the humans in this world is one called Spring. Spring is used to create web sites to make day-to-day activities more efficient. The Guild, where adventurers gather, utilizes these web sites to manage their resources. But here’s the problem. The people themselves can only use it to an extent. The results of their magic isn’t of the best quality. Their magic is unstable; the mantra is difficult to maintain. If the creator of said magic were to perish, nobody else would be able to understand what they have created. Therefore, [] is enlisting your aid in fixing the system that they have right now.

To your understanding, the magic in this world are akin to programs in your world. The mantra is the code and the result of the magic is the application. With your Advanced Programming knowledge, you agree to help. Now you begin your adventure in another world with the knowledge that you have…

## Magic of Spring

Unfortunately, it appears the whole summoning process has damaged your memory somewhat. You feel like something called a design pattern should help you in your adventure. You just can’t seem to recall what it was. []tells you that you will regain your memory in time. 

[] introduces you to the guild master. The guild master shows you the spring magic used by the guild. They explain all the basic mantra needed to create a product with spring. 

The main application is as follows:

```java
@SpringBootApplication
public class Tutorial0Application {

    public static void main(String[] args) {
        SpringApplication.run(Tutorial0Application.class, args);
    }

}

```

You can see several annotations in the code. These annotations are used to mark parts in the code. The annotation `@SpringBootApplication` shows that the class below it is a main Spring class that is used to run the application.

You recall a fragment of your memory on how Spring works in your world. A controller exists to handle requests and responses. The main logic of the application is tied to a model. To show that model to the users, a view is needed. The letters MVC flash in your head, but you forget their significance.

For now, you understand that a controller will do its work if there is a request from the application, and will return a response to that request. 
The controller is as follows:

```java
@Controller
public class MainController {

    @RequestMapping(method = RequestMethod.GET, value = "/")
    private String home() {
        return "home";
    }

}
```

A controller class is given the annotation `@Controller`.

Controller classes will have methods that return strings. In your mind, you remember the strings here represents a .html file name located in the resources/templates folder. All .html files within that folder will become the face of the application.

The home method returns the string home, which points to the home.html file in the templates folder.

The home method itself has the `@RequestMapping` annotation. You recall in addition to classes, annotations can also be given to methods and attributes.

`RequestMapping` itself functions as a manager to handle requests. This includes which method (POST, GET, PUT, DELETE, etc.) and the url handled by the method.

The home method itself has a `RequestMethod.GET` and a value of “/”. This means the url path “/” with the method GET will be accessed by the home method.

You now recall more about the controller and it’s usage with url and methods.

```java
 @RequestMapping(method = RequestMethod.GET, value = "/greet")
    private String greetingsWithRequestParam(@RequestParam("name")String name, Model model) {
        model.addAttribute("name", name);
        return "home";
    }
```

The `@RequestParam` annotation is to mark a url can be added with “?parameterName=something”. This means you can add a parameter to the “/greet” above, and it becomes “/greet?name=something”. The value in `@RequestMapping` is the name of the expected parameter in the request. `RequestMapping(required=false)` can be added to allow the variable to be optional. The default required value is true. You can see in the code above that the method greetingsWithRequestParam will need the parameter “name” to request to the url.
`String name` is the parameter name to the method, and doesn’t have to have the same name as the RequestParameter. When you open localhost:8080, you will see the contents of home.html

## Adventurer Power Level

After showing how spring works, the guild master also shows you an application used for calculating an adventurer’s strength in the guild, in hopes of jogging your memory.

```java
@Controller
public class AdventurerController {

    @Autowired
    private AdventurerCalculatorService adventurerCalculatorService;

    @RequestMapping("/adventurer/countPower")
    private String showAdventurerPowerFromBirthYear(@RequestParam("birthYear")int birthYear, Model model) {
        model.addAttribute("power", adventurerCalculatorService.countPowerPotensialFromBirthYear(birthYear));
        return "calculator";
    }
}
```

Several words trigger your recollection. Specifically, service and Autowired.

You notice that the contents of the service folder include the interface `AdventurerCalculatorService`, and the class that implements said interface `AdventurerCalculatorServiceImpl`

`AdventurerCalculatorService` is just a normal interface. But you also notice an annotation in the `AdventurerCalculatorServiceImpl` class.

```java
@Service
public class AdventurerCalculatorServiceImpl implements AdventurerCalculatorService
```

The class that implements the interface has the `@Service` annotation. Seeing that, you remember the purpose of `@Autowired`

`@Autowired` will automatically find the class declared by the implementer. The initiation is done using the annotation. The concept feels very familiar to you. But you can’t seem to remember why. The guild master also doesn’t understand why this annotation is used. The application was created a long time ago by a hero that taught them the magic of spring.

Your eyes are directed towards the parameter in the controller. In the example application before, you also see a parameter called model. The guild master explains to you the purpose of the model parameter.

Model is used to pass data to the view. In other words, to the html file. This means that the model will have a variable called “power”, and the value will be the result of the function `countPowerPotensialFromBirthYear`.

```java
@Override
    public int countPowerPotensialFromBirthYear(int birthYear) {
        int rawAge = getRawAge(birthYear);
        if (rawAge<30) {
            return rawAge*2000;
        } else if (rawAge <50) {
            return rawAge*2250;
        } else {
            return rawAge*5000;
        }
    }

    private int getRawAge(int birthYear) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        return currentYear-birthYear;
    }
```

The function above will receive an input, and calculate the power of the adventurer. The result of that calculation will be sent to the html.

```html
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org">
  <head>
    <meta charset="UTF-8" />
    <title>Adventurer Calculator</title>
  </head>
  <body>
    <h2>Welcome</h2>
    <h3 th:text="${'the power is ' + power}">to the guild</h3>
  </body>
</html>
```

To visualize the data sent from the controller, you will need a different kind of magic.

That magic is known as the template engine. The guild uses Thymeleaf as their template engine. The basic use is as you can see it.

You declare `xmlns:th=”http://www.thymeleaf.org”` to begin using thymeleaf.

Then `th:text` tag is used to change the text of the tagged part with whatever text is in `th:text`

To call the variable sent from the controller, you can generally use `th:[th operation ex:text]=”${variable-name}”`

To insert a different string, you can use `th:[th operation]=”$’other string’ + variable-name}”`

Now you’re interested in seeing the html of the previous base application.

```html
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org">
  <head>
    <meta charset="UTF-8" />
    <title>Welcome</title>
  </head>
  <body>
    <h1>Welcome</h1>
    <h2 th:if="${name !=null}" th:text="${'Hello ' + name}"></h2>
  </body>
</html>
```
You can see the use of `th:if`, and the guild master tells you it’s for conditionals.

If the condition in `th:if` is fulfilled, only then the tag will be created. You ask for permission to play around with the old system, and the guild master allows it.

You feel that if the power level number is displayed as is, it doesn’t have much value. You decide to make a small change to make that number more significant.

1. If the power level is between 0 – 20000, add the text ‘C class’ next to the power. Add to the html.
2. If the power level is between 20000 – 100000, add the text ‘B class’.
3. If the power level is above 100000, add the text ‘A class’.

To make it cleaner, the one doing the classification will be spring.

Thymeleaf will only be used to show the data. Please use the repository provided by the guild master to make the implementation. To access the repository, guild master teaches you the magic art that manipulates time and space.

## The Art of Git

Guild master shows you the capabilities of time-space magic called git. Git is a magic that can store created mantra. You can turn back time to view the mantra stored within it. You’re going to learn Git from the basics, so get ready!

1. The first step is to obtain that power. If you never use git before, you can get the power [here](https://git-scm.com/download/)
2. Then create a new folder anywhere you like and named it `the-art-of-git`.
3. If you access it with Windows, run `git bash`. If you run it through Linux or Mac, You can use your favorite `shell` (ex:  `bash`, `zsh`).
4. Open `the-art-of-git` folder with the tools that we have mentioned before (point 3). Now you will learn the power of git through text-based commands.
5. After you access the folder, please execute `git init`. That command will make that folder become a Git repository (repo), this is where you will put many kinds of code spell.
6. You can execute `git status` to know the current status of the repo. This includes any modification, new files, deleted files, that occurs to that repository.

Congratulations! You have succeeded in the first steps in using Git. The next step is to tell Git your identity.

1. Adjust your _username_ and _email_, which will be used to identify yourself when doing a _commit_. If you have done this before, you may skip the configuration steps below.
2. Execute the command `git config user.name <Your Name>` and `git config user.email <Your Email>. As an example: `git config user.name “Yotsuba Nakano”` and `git config user.email “yotsuba.nakano@gmail.com”`.
3. If you want to configure your identity globally so you don’t have to reconfigure _username_ and _email_ every time you create a new repository, add the _flag_ `--global` when executing `git config`. Example: `git config --global user.name “Yotsuba Nakano”` and `git config --global user.email “yotsuba.nakano@gmail.com”`.

Now, let’s learn the true power of this magic, saving code spells.

1. Create a _file_  named README.md in the `the-art-of-git` folder and write data with this format.
```text
 Nama : <Your Name>
  NPM : <Your Student ID>
 ```
2. Execute `git status` and you will see an _untracked file_.
3. Next, please execute `git add README.md`.
4. Again, execute the `git status` command and you’ll see a difference in the _README.md__ file. It now has `Changes to be committed` status. That means, _README.md_ will be added on the next _commit_ process. 
5. To save the changes, execute `git commit -m <Commit message>` where Commit Message is a message that represents the change that have made within that commit. For this case, you have added a files called README.md, then the commit message will be `git commit -m “Add README.md” `.

Caution:

	In the case of you execute `git commit` without flag `-m` and trapped in the dark magic of VIM, then welcome to the abyss!

- If you just want to quit the VIM, press the button `esc` to return to the command mode. Type `:q` then press `enter`. If this doesn't work, try `:q!` then enter. Now you can try to commit with `git commit -m “<Commit Message>” ` command.

- If you want to create commit message first in VIM before going out, you can press `i` to enter the insert mode. Write your commit message here. You can press `esc`to return to the command mode. Type `wq` then press enter. Your commit now saved. 

6. After you done creating commit, you can see all the commits you have done before by using `git log` command. 

Congratulations! You have learned to use one of Git’s abilities, which is to store mantra. Now you can share the repository containing the mantra with the following steps.

1. Open [gitlab.com](https://gitlab.com), and login or register your account.
2. Create a new repository named **The Art of Git** and make sure the _visibility_ is set to **Public**.
3. Click on the _clone_ button in the repository. You will be presented with two URL options, SSH and HTTPS. Copy the HTTPS URL.
4. Afterwards, in the `the-art-of-git` folder, execute the command `git remote add origin <URL>` to add your Gitlab repository as a _remote repository_. Example: `git remote add origin https://gitlab.com/yotsubanakao/the-art-of-git.git` . As a note, `origin` is the alias of your repository. You can check all available _remote_ repositories with the command `git remote-v`
5. To store a _commit_ into your Gitlab repository, execute the command `git push origin master`. The `git push` command will ‘push’ all the _commits_ in _local_ that hasn’t been added to the _remote_ repository. `origin` means you are pushing to the repository with the alias `origin`. `master` means you are pushing to the branch `master` in the `origin` repository.

Now you can view the changes you have made in your Gitlab repository.

## Alternate Universe

You will continue to use this repository to learn even more about the powers of Git.

1. Clone this repository by executing the command `git clone <URL repo>`.
2. Check the _remote url_ using the command `git remote -v`. You will see this repository has the url _remote_ `origin`. 
3. Change the name `origin` into `upstream` with the command `git remote rename origin upstream`.
4. Create a new Gitlab repository. You are free to name it whatever you want. Make sure the visibility is set to public. For this example, let’s say you named your repository `AP 2020`.
5. Copy the HTTPS URL of the repository you just made, just like the previous one. Add the remote origin with the command `git remote add origin <URL REPO>`. Example: `git remote add origin https://gitlab.com/yotsubanakano/ap-2020.git`. 
6. Recheck your remote URL with the command `git remote -v`. You will see two remote url, `origin` and `upstream`.
7. Execute the command `git push origin master` to push the repository into your Gitlab repository.

Next, you will learn another one of Git’s capabilities, branching. Branching is a power that can create an alternate universe inside your repository. All changes in one side branch will not affect the main branch.

1. Run **IntelliJ IDEA** and open this repository folder.
2. Open the file [`Tutorial0Application.java`](src/main/java/id/ac/ui/cs/tutorial0/Tutorial0Application.java) and you will see the `run` button, a green button next to the `Tutorial0Application` class. Run the Spring application, and you can access it using the address `http://localhost:8080`.
3. Create a new branch using the command `git checkout -b tutorial-0`. `git checkout` is a command you use to change to a different branch. The flag `-b` is used to create a new branch. `tutorial-0` is the name of the branch you are creating. You can check all the branches in your repository with the command `git branch -v`.
4. Open the file [`MainController.java`](src/main/java/id/ac/ui/cs/tutorial0/controller/MainController.java), and then change the return value in the **home** method as follows.

```diff
...

private String home() {
-   return "home";
+   return "house";
}

...
```

The red line means the mantra on that line is removed, and changed with the mantra on the green line. Create a commit for the change above.
5. To push your changes to the `tutorial-0` branch, use the command `git push origin tutorial-0`.

Congratulations! You have successfully created a change in the `tutorial-0` branch. Now you will change something again, but this time in the master branch.

1. You can return to the master branch by executing the command `git checkout master`.
2. Still in the file [`MainController.java`](src/main/java/id/ac/ui/cs/tutorial0/controller/MainController.java), change the return value in the **home** method as follows.

```diff
...

private String home() {
-   return "home";
+   return "rumah";
}

...
```

Create a different commit for the change above.

3. Now you can merge the changes in the `tutorial-0` branch into the `master` branch by executing the command `git merge tutorial-0`, where `tutorial-0` is the name of the branch you want to merge.

Congratulations, you have now created what is called a merge conflict. Of course normally you don’t want this to happen, but it will quite often when using Git. Now it’s time for you to correct the timeline by fixing the inconsistency caused by the difference in mantra from two different branches or timelines.

Once again open the file [`MainController.java`](src/main/java/id/ac/ui/cs/tutorial0/controller/MainController.java) which now has a conflict inside it. You will see the contents of the file as follows.

```diff
...

private String home() {
<<<<<<< HEAD
    return "rumah";
=======
    return "house";
>>>>>>> tutorial-0
}

...
```

Notice there are now two parts that have branch names as labels, `HEAD` and `tutorial-0`. The mantra contained in `HEAD` are the changes that happened in the `master` branch, while the mantra in between `===` and `tutorial-0` are the changes that happened in the `tutorial-0` branch. To solve this conflict, there are three options available to you.
1. Accept the change in `master` and delete the change from `tutorial-0`.

```java
...

private String home() {
    return "rumah";
}

...
```

2. Accept the change in `tutorial-0` and delete the change from `master`.

```java
...

private String home() {
    return "house";
}

...
```

3. Combine both changes, or create a new change.

```java
...

private String home() {
    return "rumah house";
}

...
```

Once you have solved the conflict, create a new commit and push the result to the `master` branch of your Gitlab repository.

## Back To The Past

Try halting and re-running your Spring application. Reopen `http://localhost:8080` and you will see an error. How can we get it working again?

Don’t worry, you can use another ability of Git, which is to turn back time to when your mantra was working fine.

1. Execute the command `git log` to see all the past changes in your repository. You will see something similar to this.

```text
commit bf02fdc2ffd8876c1d911bdc9527ad68adb0dd6d (HEAD -> master, origin/master)
Merge: 6876365 30e94af
Author: Yotsuba Nakano <yotsuba.nakano@gmail.com>
Date:   Sun Jan 26 22:51:07 2020 +0700

    fix conflict

commit 68763658f43d8226cd46647ea681794fc4d7d393
Author: Yotsuba Nakano <yotsuba.nakano@gmail.com>
Date:   Sun Jan 26 22:31:07 2020 +0700

    Change home to rumah

commit 30e94afb32ea21e4bff78c93879e85b3831e75e2 (tutorial-0)
Author: Yotsuba Nakano <yotsuba.nakano@gmail.com>
Date:   Sun Jan 26 22:15:06 2020 +0700

    Change home to house

commit 7a52d6f3984b95c5c0e807580c260562eed37aae (upstream/master, upstream/HEAD)
Author: adrika-novrialdi <adrika.novrialdi@gmail.com>
Date:   Sat Jan 25 13:10:39 2020 +0700

    Add tutorial 0 readme

commit 832c65b3ec50ec599b90291fd2b838204e752e33
Author: adrika-novrialdi <adrika.novrialdi@gmail.com>
Date:   Sat Jan 25 13:10:12 2020 +0700

    Create basic spring application for tutorial

commit 48785ab2deab9b702ae227168bed90de4e71a547
Author: adrika-novrialdi <adrika.novrialdi@gmail.com>
Date:   Fri Jan 24 18:54:40 2020 +0700

    Create Tutorial 0 Application

commit 01c4ac473491c21540b71a6d69540e3002a82ce2
Author: adrika-novrialdi <adrika.novrialdi@gmail.com>
Date:   Fri Jan 24 16:06:44 2020 +0700

    Init AdvProg Tutorial Repository

```

2. Take a look at each commit and try to find a commit where you feel your application was working just fine. Use the command `git checkout <commit hash>` to change to that commit. Example: `git checkout 7a52d6f3984b95c5c0e807580c260562eed37aae`.
3. Now you can try running your application again. If it is running normally again and you feel that commit is the correct one, you can copy the commit hash and go back to `HEAD` with the command `git checkout master`.
4. You are going to do a revert to the chosen commit with the command `git revert <commit hash>. Example: `git revert 7a52d6f3984b95c5c0e807580c260562eed37aae`. The revert will create a new commit that shows a new change.
5. Push the commit once more to your Gitlab repository.

You may have heard of another one of Git’s capabilities similar to revert, called reset. Both have the ability to undo changes, but revert is a much safer option if the change has already been pushed to an online repository. revert creates a new commit to undo changes, while reset deletes commits to undo changes.

## Checklist

- [ ] Read this tutorial
- [ ] Implement powerClassifier
- [ ] Make sure the controller and html (thymeleaf) displays the data correctly
- [ ] Create a new repository `the-art-of-git` like in the example
- [ ] Create a new branch `tutorial-0` and make changes according to the example, and push to your Gitlab repository
- [ ] Make changes to the `master` branch, merge the `tutorial-0` branch, and resolve the conflict like in the example
- [ ] Push the result of the conflict resolution to your Gitlab repository, and revert to a commit that doesn’t have any problems like in the example.
